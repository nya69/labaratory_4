"""Иванченко Анастасии КИ19-17 / 2б
Вариант 2 'О Триграммах'
Необходимо найти в триграмму(три буквы), которые встречаются в этой
строке наибольшее количество раз как подстрока.  """



def checkout(strin):
    """Проверка ввода.
    Функция принимает строку неупорядоченных символов.
    Проверяет введённые символы на кодировку(65-96)-
    большие английские буквы.
    Сиволы не входящие в английский алфавит не обрабатываются.

    Args:
        strin: строка неупорядоченных символов.

    Returns:
        Строка с большими английскими буквами.

    Raises:
        SystemExit.

    Examples:
        >>> checkout('GHYJGHYTGHYM')
        'GHYJGHYTGHYM'
        >>> checkout('qwertyuiop')
        'QWERTYUIOP'
        >>> checkout('GHYJ0TG9YM')
        Traceback (most recent call last):
            ...
        SystemExit: None
        """

    strin = strin.upper()
    for i in strin:
        kod_symbol = ord(i)
        if not 64 < kod_symbol < 97:
            print('Великобританскую букву!!!!!')
            exit()
        else:
            valid_strin = strin
    return valid_strin


def trigram(strin):
    """ Поиск триграмм.
    Функция принимает на вход строку неупорядоченных символов.
    Определяет триграмму - три символа, которые находятся рядом
    и встречаются больше всего в введенной строке.
    Пустая строка и меньше двух символов не обрабатываются.

    Args:
        strin: строка неупорядоченных символов.

    Returns:
        Строку из трёх символов - триграмму.

    Raises:
        ValueError.

    Examples:
        >>> my_sort('GHYJGHYTGHYM')
        'GHY'
        >>> my_sort('dfghyc')
        ['dfg', 'fgh', 'ghy', 'chy']
        >>> trigram('=-=-%$$$$%###$%')
        ['$$%', '$$$', '#$%']
        >>> my_sort(' ')
        Traceback (most recent call last):
            ...
        ValueError: max() arg is an empty sequence

    """
    symbol = {}
    strin = list(strin)
    # обход строки
    for i in range(len(strin) - 2):
        # деление строки на 3 буквы, сортировка букв
        lett = ''.join(sorted(strin[i:3 + i]))
        # подсчёт частоты встречаемости тройки букв
        symbol[lett] = symbol.get(lett, 0) + 1
    cask = []  # контейнер для триграммы
    max_repeat = max(symbol.values())
    list_keys = list(symbol.keys())
    for i in range(len(symbol)):
        if symbol[list_keys[i]] == max_repeat:
            cask.append(list_keys[i])
    return cask


# if __name__ == "__main__":
#     seq = input('Введите строку ')
#     # print(trigram(checkout(seq)))
#     print(trigram(seq))
