from pract4 import trigram
import pytest

#тесты на сортировку
def test_good():
    '''Поиск одной триады.
    Находит в правильной последовательности букв, триаду
    встречающуюся чаще всего. '''
    assert trigram('GHYJGHYTGHYM') == ['GHY']
    assert trigram('DFT') == ['DFT']


def test_several():
    '''Поиск триад в последовательности букв.
    Находит несколько триад, которые встречались
    в последовательности чаще всего.'''
    assert trigram('GHUYJGHUYJGHUYJ') == ['GHU', 'HUY', 'UYJ']
    assert trigram('dfghyc') == ['dfg', 'fgh', 'ghy', 'chy']
    assert trigram('настянастя') == ['анс', 'аст', 'стя']


def test_magic():
    '''Поиск триады в последовательности непонятных символов.
    Работает аналагично двум первым сортировкам. '''
    assert trigram('=-=-%$$$$%###$%') == ['$$%', '$$$', '#$%']
    assert trigram('567856784') == ['567', '678']


def test_rais():
    '''Неправильный ввод данных.
    Данные, которые не проходят через функцию по поиску триад.'''
    with pytest.raises(ValueError):
        trigram(' ')
        trigram('D')
        trigram('RT')
