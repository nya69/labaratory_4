from pract4 import checkout
import pytest
# тесты на проверку


def test_normal():
    '''Ввод правильных значений.
    Все английские буквы, не важно в каком колличестве,
    проходят данный тест. '''
    assert checkout('GHYJGHYTGHYM') == 'GHYJGHYTGHYM'
    assert checkout('G') == 'G'
    assert checkout('qwertyuiop') == 'QWERTYUIOP'

def test_straing():
    '''Ввод неправильных значений.
    Все буквы не входящие в кодовый диапазон, а именно все символы, кроме
    английского алфавита, яваляются ошибочными данными. '''
    with pytest.raises(SystemExit):
        checkout('GHYJGHYTG9YM')
        checkout(' ')
        checkout('фывапро')
        checkout('fghj8yhj')
        checkout('=')
